#!/bin/sh

INSTANCE_NAME=${1}
INSTANCE_PUBLIC_IP="$(gcloud compute instances describe "$INSTANCE_NAME" --format json | jq .networkInterfaces[].accessConfigs[].natIP | tr --delete '\"')"
OS_LOGIN_USER=$(gcloud compute os-login describe-profile --format json | jq .posixAccounts[].username | tr --delete '"')

ssh -o StrictHostKeyChecking=false ${OS_LOGIN_USER}@${INSTANCE_PUBLIC_IP}
