resource "google_compute_firewall" "allow_external" {
  name    = "allow-external"
  network = "${google_compute_network.kubernetes.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "443", "6443"]
  }

  source_ranges = ["0.0.0.0/0"]
}
