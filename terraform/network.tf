resource "google_compute_network" "kubernetes" {
  name                    = "kubernetes"
  auto_create_subnetworks = false
}
