resource "google_compute_http_health_check" "internal_health_check" {
  name = "internal-service-health-check"
  description = "Kubernetes Health Check"
  timeout_sec        = 1
  check_interval_sec = 1

  # At some point convert this to HTTPS
  host = "kubernetes.default.svc.cluster.local"
  request_path = "/healthz"
}
