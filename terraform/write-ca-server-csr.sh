#!/bin/sh

set -e

EXTERNAL_IP=$(curl -sH "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
INTERNAL_IP=$(curl -sH "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/ip)
ORGANIZATION=$(curl -sH "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/project/attributes/organization)

cat <<EOF | tee /tmp/ca-server-csr.json
{
  "hosts": [
    "ca",
    "localhost",
    "127.0.0.1",
    "${EXTERNAL_IP}",
    "${INTERNAL_IP}"
  ],
  "key": {
    "algo": "ecdsa",
    "size": 384
  },
  "names": [
    {
      "O": "${ORGANIZATION}"
    }
  ]
}
EOF

sudo chown cfssl:cfssl /tmp/ca-server-csr.json
sudo mv /tmp/ca-server-csr.json /etc/cfssl/ca-server-csr.json
