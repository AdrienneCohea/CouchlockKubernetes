#!/bin/sh
set -e

ACCOUNT_FILE=$(stat -c %n ~/.gcloud/account.json)
PROJECT_ID=$(gcloud config get-value project)
REGION=$(gcloud config configurations describe default --format json | jq .properties.compute.region | tr --delete '"')
OS_LOGIN_USER=$(gcloud compute os-login describe-profile --format json | jq .posixAccounts[].username | tr --delete '"')
SSH_PRIVATE_KEY_PATH=$(stat -c %n ~/.ssh/id_ed25519)
COMMIT=$(git rev-parse HEAD | cut -c 1-16)
CA_SHARED_KEY=$(cat ca-shared-key.txt)
ENVIRONMENT=production

terraform destroy \
    -auto-approve \
    -var account_file_path=${ACCOUNT_FILE} \
    -var project=${PROJECT_ID} \
    -var region=${REGION} \
    -var ssh_user=${OS_LOGIN_USER} \
    -var ssh_private_key_path=${SSH_PRIVATE_KEY_PATH} \
    -var commit=${COMMIT} \
    -var environment=${ENVIRONMENT} \
    -var ca_shared_key=${CA_SHARED_KEY}
