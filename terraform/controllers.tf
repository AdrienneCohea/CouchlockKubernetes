resource "google_compute_instance" "controllers" {
  count        = 3
  name         = "controller${count.index}"
  machine_type = "n1-standard-1"
  zone         = "us-west1-a"

  tags = ["k8s", "controller", "${var.environment}"]

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.k8s_control_plane_image.self_link}"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.kubernetes.self_link}"
    network_ip = "${google_compute_subnetwork.kubernetes.gateway_address}${count.index}"

    access_config { }
  }

  can_ip_forward = true

  service_account {
    scopes = ["compute-rw", "storage-ro", "service-management", "service-control", "logging-write", "monitoring"]
  }

  connection { user = "${var.ssh_user}" private_key = "${file(var.ssh_private_key_path)}" }

  provisioner "file" { content = "${file("${path.module}/ca.pem")}" destination = "/tmp/ca.pem" }
  provisioner "file" { content = "${file("${path.module}/kubernetes-csr.json")}" destination = "/tmp/kubernetes-csr.json" }
  provisioner "file" { content = "${data.template_file.cfssl_config_json.rendered}" destination = "/tmp/cfssl-config.json" }

  provisioner "remote-exec" {
    inline = [
      "echo $(gcloud compute instances list --filter \"name = certificate-authority\" --format \"value(networkInterfaces[0].networkIP)\") ca | sudo tee -a /etc/hosts",
      "sudo mv /tmp/ca.pem /usr/local/share/ca-certificates/root.crt",
      "echo \"ETCD_NAME=${self.name}\nINTERNAL_IP=${self.network_interface.0.network_ip}\" | sudo tee /etc/etcd.environment",
      "systemctl status kubernetes-pki.service && rm /tmp/kubernetes-csr.json"
    ]
  }
}

data "template_file" "cfssl_config_json" {
  template = "${file("${path.module}/cfssl-config.json")}"

  vars {
    ca_shared_key = "${var.ca_shared_key}"
  }
}
