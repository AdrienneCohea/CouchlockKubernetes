resource "google_compute_instance" "workers" {
  count        = 3
  name         = "worker${count.index}"
  machine_type = "n1-standard-1"
  zone         = "us-west1-a"

  tags = ["k8s", "worker", "production"]

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.k8s_control_plane_image.self_link}"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.kubernetes.self_link}"
    network_ip = "10.240.0.2${count.index}"

    access_config {
    }
  }

  can_ip_forward = true

  metadata {
    pod-cidr = "10.200.${count.index}.0/24"
  }

  service_account {
    scopes = ["compute-rw", "storage-ro", "service-management", "service-control", "logging-write", "monitoring"]
  }

  # Copy CA certificate
  provisioner "file" {
    connection {
      user     = "${var.ssh_user}"
      private_key = "${file(var.ssh_private_key_path)}"
    }

    content     = "${file("${path.module}/ca.pem")}"
    destination = "/tmp/ca.pem"
  }

  # Copy Kubernetes API TLS key
  provisioner "file" {
    connection {
      user     = "${var.ssh_user}"
      private_key = "${file(var.ssh_private_key_path)}"
    }

    content     = "${file("${path.module}/kubernetes-key.pem")}"
    destination = "/tmp/kubernetes-key.pem"
  }

  # Copy Kubernetes API TLS certificate
  provisioner "file" {
    connection {
      user     = "${var.ssh_user}"
      private_key = "${file(var.ssh_private_key_path)}"
    }

    content     = "${file("${path.module}/kubernetes.pem")}"
    destination = "/tmp/kubernetes.pem"
  }

  # Move configuration files into place
  provisioner "remote-exec" {
    connection {
      user     = "${var.ssh_user}"
      private_key = "${file(var.ssh_private_key_path)}"
    }

    inline = [
      "sudo mv /tmp/ca.pem /etc/etcd/ca.pem",
      "sudo mv /tmp/kubernetes-key.pem /etc/etcd/kubernetes-key.pem",
      "sudo mv /tmp/kubernetes.pem /etc/etcd/kubernetes.pem",
      "echo \"ETCD_NAME=${self.name}\nINTERNAL_IP=${self.network_interface.0.network_ip}\" | sudo tee /etc/etcd.environment"
    ]
  }
}
