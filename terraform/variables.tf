variable "account_file_path" {}
variable "project" {}
variable "region" {}
variable "ssh_user" {}
variable "ssh_private_key_path" {}
variable "commit" {}
variable "environment" { default = "development" }
variable "ca_shared_key" { }
variable "ca_organization_name" { default = "Antifascist Security Group" }
