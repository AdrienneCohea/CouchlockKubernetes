resource "google_compute_firewall" "allow_health_check" {
  name    = "allow-health-check"
  network = "${google_compute_network.kubernetes.name}"

  allow {
    protocol = "tcp"
  }

  source_ranges = [
    "209.85.152.0/22",
    "209.85.204.0/22",
    "35.191.0.0/16"
  ]
}
