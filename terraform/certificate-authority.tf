resource "google_compute_instance" "certificate-authority" {
  name         = "certificate-authority"
  machine_type = "g1-small"
  zone         = "us-west1-a"
  tags         = ["certificate-authority", "${var.environment}"]

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.k8s_certificate_authority_image.self_link}"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.kubernetes.self_link}"
    access_config { }
  }

  can_ip_forward = true

  service_account {
    scopes = ["compute-rw", "storage-ro", "service-management", "service-control", "logging-write", "monitoring"]
  }

  connection { user = "${var.ssh_user}" private_key = "${file(var.ssh_private_key_path)}" }

  provisioner "file" { content = "${file("${path.module}/ca.pem")}" destination = "/tmp/ca.pem" }
  provisioner "file" { content = "${file("${path.module}/ca-key.pem")}" destination = "/tmp/ca-key.pem" }
  provisioner "file" { content = "${data.template_file.ca_config.rendered}" destination = "/tmp/ca-config.json" }
  provisioner "file" { content = "${file("${path.module}/write-ca-server-csr.sh")}" destination = "/tmp/write-ca-server-csr.sh" }
  
  provisioner "remote-exec" {
    inline = [
      "sudo chown cfssl:cfssl /tmp/ca.pem",
      "sudo chown cfssl:cfssl /tmp/ca-key.pem",
      "sudo chown cfssl:cfssl /tmp/ca-config.json",
      "bash /tmp/write-ca-server-csr.sh",
      "sudo mv /tmp/ca.pem /etc/cfssl/ca.pem",
      "sudo mv /tmp/ca-key.pem /etc/cfssl/ca-key.pem",
      "sudo mv /tmp/ca-config.json /etc/cfssl/config.json"
    ]
  }
}

data "template_file" "ca_config" {
  template = "${file("${path.module}/ca-config.json")}"

  vars {
    ca_shared_key = "${var.ca_shared_key}"
  }
}
