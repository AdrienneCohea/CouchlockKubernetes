resource "google_compute_target_pool" "default" {
  name = "instance-pool"

  health_checks = [
    "${google_compute_http_health_check.internal_health_check.name}",
  ]
}
