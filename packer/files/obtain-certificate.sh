#!/bin/bash

CONFIG_PATH=${1}
CSR_PATH=${2}
KEY_PATH=${3}
CERT_PATH=${4}

TEMP_DIR=$(mktemp -d)

set -e

cd ${TEMP_DIR}

cfssl gencert -profile=kubernetes -config=${CONFIG_PATH} ${CSR_PATH} | cfssljson -bare output

sudo mv output-key.pem ${KEY_PATH}
sudo mv output.pem ${CERT_PATH}

cd ..
rm -rf ${TEMP_DIR}
