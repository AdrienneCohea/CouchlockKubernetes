#!/bin/sh
set -e

ACCOUNT_FILE=$(stat -c %n ~/.gcloud/account.json)
PROJECT_ID=$(gcloud config get-value project)
COMMIT=$(git rev-parse HEAD | cut -c 1-16)
OS_LOGIN_USER=$(gcloud compute os-login describe-profile --format json | jq .posixAccounts[].username | tr --delete '"')
SSH_PRIVATE_KEY_PATH=$(stat -c %n ~/.ssh/id_ed25519)

find . -name "*.json" -exec \
    packer validate \
    -var ssh_username=${OS_LOGIN_USER} \
    -var ssh_private_key_file=${SSH_PRIVATE_KEY_PATH} \
    -var account_file=${ACCOUNT_FILE} \
    -var project_id=${PROJECT_ID} \
    -var commit=${COMMIT} \
    -var environment=production \
    {} \;

find . -name "*.json" -exec \
    packer build \
    -var ssh_username=${OS_LOGIN_USER} \
    -var ssh_private_key_file=${SSH_PRIVATE_KEY_PATH} \
    -var account_file=${ACCOUNT_FILE} \
    -var project_id=${PROJECT_ID} \
    -var commit=${COMMIT} \
    -var environment=production \
    {} \;
