#!/bin/sh

set -e

wget -q --https-only \
    https://pkg.cfssl.org/R1.2/SHA256SUMS \
    https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 \
    https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64

sha256sum --ignore-missing --check SHA256SUMS
rm SHA256SUMS

chmod +x cfssl_linux-amd64
chmod +x cfssljson_linux-amd64

sudo mv cfssl_linux-amd64 /usr/bin/cfssl
sudo mv cfssljson_linux-amd64 /usr/bin/cfssljson

cfssl version

sync
