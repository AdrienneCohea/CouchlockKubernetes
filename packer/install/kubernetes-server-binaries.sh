#!/bin/bash

wget -q https://dl.k8s.io/${KUBERNETES_VERSION}/kubernetes-server-linux-amd64.tar.gz
tar zxf kubernetes-server-linux-amd64.tar.gz

sudo cp kubernetes/server/bin/kube-apiserver /usr/bin
sudo cp kubernetes/server/bin/kube-controller-manager /usr/bin
sudo cp kubernetes/server/bin/kube-scheduler /usr/bin
sudo cp kubernetes/server/bin/kubectl /usr/bin
sudo cp kubernetes/server/bin/kubeadm /usr/bin
rm -rf kubernetes

sync
