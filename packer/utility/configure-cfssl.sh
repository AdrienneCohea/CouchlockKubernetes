#!/bin/sh

set -e

sudo mv /tmp/cfssl.path /etc/systemd/system/cfssl.path
sudo mv /tmp/cfssl.service /etc/systemd/system/cfssl.service

sudo systemctl daemon-reload
sudo systemctl enable cfssl.path
