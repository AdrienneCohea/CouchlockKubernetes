#!/bin/bash
set -e

sudo mv /tmp/root-ca.path /etc/systemd/system/root-ca.path
sudo mv /tmp/root-ca.service /etc/systemd/system/root-ca.service
sudo mv /tmp/kubernetes-pki.path /etc/systemd/system/kubernetes-pki.path
sudo mv /tmp/kubernetes-pki.service /etc/systemd/system/kubernetes-pki.service
sudo mv /tmp/etcd.path /etc/systemd/system/etcd.path
sudo mv /tmp/etcd.service /etc/systemd/system/etcd.service
sudo mv /tmp/kube-apiserver.path /etc/systemd/system/kube-apiserver.path
sudo mv /tmp/kube-apiserver.service /etc/systemd/system/kube-apiserver.service
sudo mv /tmp/kube-controller-manager.path /etc/systemd/system/kube-controller-manager.path
sudo mv /tmp/kube-controller-manager.service /etc/systemd/system/kube-controller-manager.service
sudo mv /tmp/kube-scheduler.path /etc/systemd/system/kube-scheduler.path
sudo mv /tmp/kube-scheduler.service /etc/systemd/system/kube-scheduler.service

sudo systemctl daemon-reload
sudo systemctl enable root-ca.path
sudo systemctl enable kubernetes-pki.path
sudo systemctl enable etcd.path
sudo systemctl enable kube-apiserver.path
sudo systemctl enable kube-controller-manager.path
sudo systemctl enable kube-scheduler.path

sync
