#!/bin/bash

set -e

sudo mkdir -p /etc/kubernetes/config
sudo mv /tmp/kube-scheduler.yaml /etc/kubernetes/config/kube-scheduler.yaml

sync
