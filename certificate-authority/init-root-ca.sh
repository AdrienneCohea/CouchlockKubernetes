#!/bin/bash

[ -f ca.pem ] || (cfssl gencert -initca ca-csr.json | cfssljson -bare ca)

openssl x509 -text -noout -in ca.pem
